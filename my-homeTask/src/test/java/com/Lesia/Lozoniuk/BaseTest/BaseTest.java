package com.Lesia.Lozoniuk.BaseTest;

import com.Lesia.Lozoniuk.Listeners.EventListenerImplementation;
import com.Lesia.Lozoniuk.PageObject.CardPage;
import com.Lesia.Lozoniuk.PageObject.FinishOrderPage;
import com.Lesia.Lozoniuk.PageObject.RegistrationFormPage;
import com.Lesia.Lozoniuk.PageObject.ShopPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.testng.annotations.*;

import java.io.File;

public class BaseTest extends ConciseAPI {

    public static EventFiringWebDriver driver;
    public static WebDriver brDriver;
    public ShopPage shopPage;
    public RegistrationFormPage registrationFormPage;
    public CardPage cardPage;
    public FinishOrderPage finishOrderPage;


    EventListenerImplementation eventImplementation = new EventListenerImplementation();

    @Parameters("browser")
    @BeforeClass
    public void setBrowser(@Optional("firefox") String browser) {

        //select driver from project resources by os version (for 64 bit driver versions or default available)
        // was not tested on Linux yet

        String driverSufix = "";
        String driverPrefix = "";
        String os = System.getProperty("os.name").toLowerCase();
        System.out.println("Lookin for driver for:" + os);

        if (os.contains("win")) {
            driverPrefix = "/win/";
            driverSufix = ".exe";
        } else if (os.contains("mac")) {
            driverPrefix = "/mac/";
        }
        // TODO: have to test if it does work on Linux
        else if (os.contains("nux")) {
            driverPrefix = "/linux/";
        } else {
            System.out.println("Please modify BaseTest.java to work with " + os);
        }

        if (browser.equalsIgnoreCase("firefox")) {
            System.setProperty("webdriver.gecko.driver", new File(BaseTest.class.getResource(driverPrefix + "/geckodriver" + driverSufix).getFile()).getPath());
            System.out.println(System.getProperty("webdriver.gecko.driver"));
            brDriver = new FirefoxDriver();

        } else if (browser.equalsIgnoreCase("chrome")) {
            System.setProperty("webdriver.chrome.driver", new File(BaseTest.class.getResource(driverPrefix + "/chromedriver" + driverSufix).getFile()).getPath());
            System.out.println(System.getProperty("webdriver.chrome.driver"));
            brDriver = new ChromeDriver();
        }
        driver = new EventFiringWebDriver(brDriver);
        driver.register(eventImplementation);

        shopPage = new ShopPage(driver);
        registrationFormPage = new RegistrationFormPage(driver);
        cardPage = new CardPage(driver);
        finishOrderPage = new FinishOrderPage(driver);
    }

    @Test
    public void openShopPage() {
        driver.manage().window().fullscreen();
        driver.get(shopPage.homePage);
        waitForPageLoaded();
    }

    @AfterClass
    public static void quite() {
        driver.quit();
    }

    @Override
    public WebDriver getWebDriver() {
        return driver;
    }
}

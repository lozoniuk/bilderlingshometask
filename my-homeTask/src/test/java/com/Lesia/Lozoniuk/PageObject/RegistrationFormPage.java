package com.Lesia.Lozoniuk.PageObject;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class RegistrationFormPage extends BasePage {


    @FindBy(css = ".user-info")
    public WebElement signInButton;

    @FindBy(css = ".logout.hidden-sm-down")
    public WebElement signOutButton;

    @FindBy(css = ".no-account")
    public WebElement newAccountButton;

    @FindBy(css = "input[name*='firstname']")
    public WebElement firstNameField;

    public String firstName = RandomStringUtils.randomAlphabetic(7) +"lesia";

    @FindBy(css = "input[name*='lastname']")
    public WebElement lastNameField;

    public String lastName = RandomStringUtils.randomAlphabetic(7) + "lozoniuk";

    @FindBy(css = "input[name*='email']")
    public WebElement emailField;

    public String email = RandomStringUtils.randomAlphabetic(7) + "@gmail.com";

    @FindBy(css = "input[name*='password']")
    public WebElement passwordField;

    public String password = "qwerty";

    @FindBy(css = "button[data-link-action*='save']")
    public WebElement submitButton;

    public void registrationNewUser (){
        signInButton.click();
        newAccountButton.click();
        firstNameField.sendKeys(firstName);
        lastNameField.sendKeys(lastName);
        emailField.sendKeys(email);
        passwordField.sendKeys(password);
        submitButton.click();
    }

    public String getNameOfNewUser(){
         return signInButton.getText();
    }

    public void logOut(){
        signOutButton.click();
        waitForPageLoaded();
    }

    public RegistrationFormPage(WebDriver driver) {
        super(driver);
    }
}

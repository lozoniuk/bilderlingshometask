package com.Lesia.Lozoniuk.PageObject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOfElementLocated;

public class ShopPage extends BasePage {

    public String homePage = "http://demo.prestashop.com";

    public String frame = "framelive";

    @FindBy(css = "#category-6 a")
    public WebElement accessories;

    @FindBy(css = "section:nth-child(6) ul li:nth-child(2) span[class ='custom-checkbox']")
    public WebElement priceFilter18_20;

    @FindBy(css = "span[itemprop = 'price']")
    public List<WebElement> allPrices;

    @FindBy(css = ".h3.product-title")
    public List<WebElement> allItems;

    @FindBy(css = ".bootstrap-touchspin-up")
    public WebElement addMoreItems;

    @FindBy(css = ".add-to-cart")
    public WebElement addToCard;

    public String modalWindow = ".modal-dialog a[class *='btn']";

    @FindBy(css = ".modal-dialog a[class *='btn']")
    public WebElement proceedToCheckoutButton;

    public void switchToFrame() {
        getWebDriver().switchTo().frame(frame);
    }

    public void selectProductWithPrice18_20() {
        accessories.click();
        scrollToElement(priceFilter18_20);
        priceFilter18_20.click();
        waitForPageLoaded();
    }

    public List<Double> grabAllPrices() {

        List<Double> allPricesList = new ArrayList<Double>();

        for (WebElement prices : allPrices) {
            String price = prices.getText();
            price = price.substring(1);
            double priceValue = Double.parseDouble(price);
            allPricesList.add(priceValue);
        }
        return allPricesList;
    }

    public void addToCard(int itemsNumber) {

        Random rand = new Random();
        int randomIndex = rand.nextInt(allItems.size());
        allItems.get(randomIndex).click();

        for (int i=1; i< itemsNumber; i++){
            addMoreItems.click();
        }
        addToCard.click();
        (new WebDriverWait(getWebDriver(), 5)).until(visibilityOfElementLocated(By.cssSelector(modalWindow)));
        proceedToCheckoutButton.click();
        waitForPageLoaded();
    }

    public void navigateBack(int times) {

        for (int i=1; i<=times;i++){
            getWebDriver().navigate().back();
        }
        waitForPageLoaded();
        //Necessary to work correct on Firefox
        getWebDriver().switchTo().defaultContent();
        getWebDriver().switchTo().frame(frame);
        waitForPageLoaded();
    }

    public ShopPage(WebDriver driver) {
        super(driver);
    }
}

package com.Lesia.Lozoniuk.PageObject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class CardPage extends BasePage {

    @FindBy(css = ".current-price")
    public WebElement priceOnCard;

    @FindBy(css = ".col-md-6.col-xs-2.price")
    public WebElement priceTotal;

    @FindBy(css = ".col-md-6.col-xs-2.price")
    public List<WebElement> priceAll;

    @FindBy(css = "#cart-subtotal-products span[class ='value']")
    public WebElement priceAllItems;

    @FindBy(css = "#cart-subtotal-shipping span[class ='value']")
    public WebElement shippingPrice;

    @FindBy(css = ".cart-summary-line.cart-total span[class ='value']")
    public WebElement priceTotalOnTheCard;

    @FindBy(css = "#payment-option-1-additional-information > section > dl > dd:nth-child(2)")
    public WebElement priceInPaymentByCheck;

    @FindBy(css = ".text-sm-center a[class *='btn']")
    public WebElement toCheckoutButton;


    public double getPriceAllItems() {

        return Double.parseDouble(priceAllItems.getText().substring(1));
    }

    public double getShippingPrice() {

        return Double.parseDouble(shippingPrice.getText().substring(1));
    }


    public double getPriceTotalOnTheCard() {

        return Double.parseDouble(priceTotalOnTheCard.getText().substring(1));
    }

    public double getPriceInPaymentByCheck() {

        String price = priceInPaymentByCheck.getText();
        return Double.parseDouble(price.substring(1, price.length() - 12));
    }

    public double priceOfItem() {
        return Double.parseDouble(priceOnCard.getText().substring(1));
    }

    public double priceTotal() {
        return Double.parseDouble(priceTotal.getText().substring(1));
    }

    public double checkPriceTotalOnTheCard() {

        double priceSum = 0.0;

        for (WebElement prices : priceAll) {
            String price = prices.getText();
            price = price.substring(1);
            double priceValue = Double.parseDouble(price);
            priceSum += priceValue;
        }
        return priceSum;
    }

    public void proceedToCheckout() {
        toCheckoutButton.click();
    }

    public CardPage(WebDriver driver) {
        super(driver);
    }
}

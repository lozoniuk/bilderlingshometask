package com.Lesia.Lozoniuk.PageObject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class FinishOrderPage extends BasePage {

    @FindBy(css = "input[name *='address1']")
    public WebElement fieldAddress;

    public String address = "latvia";

    @FindBy(css = "input[name *='city']")
    public WebElement fieldCity;

    public String city = "Riga";

    @FindBy(css = "input[name *='postcode']")
    public WebElement fieldPostcode;

    public String postcode = "32345";

    @FindBy(name = "id_state")
    public WebElement dropdownChooseState;

    @FindBy(css = ".form-footer button[type *='submit']")
    public WebElement continueButton1;

    @FindBy(css = "#js-delivery > button")
    public WebElement continueButton2;

    @FindBy(id = "payment-option-1")
    public WebElement paymentByCheck;

    @FindBy(css = "#payment-confirmation > div.ps-shown-by-js > button")
    public WebElement continueButtonLast;

    @FindBy(id = "conditions_to_approve[terms-and-conditions]")
    public WebElement iAgreeButton;

    @FindBy(css = "#order-details ul")
    public WebElement orderDetails;

    @FindBy(css = "#order-details ul li:nth-child(1)")
    public WebElement orderCode;

    public String orderPaymentText = "Payment method: Payments by check";
    public String orderShippingText = "Shipping method: My carrier";
    public String orderDeliveryText = "Delivery next day!";

    public void fillFormToCheckout() {

        fieldAddress.sendKeys(address);
        fieldCity.sendKeys(city);
        fieldPostcode.sendKeys(postcode);
        selectFromDropdown(dropdownChooseState, 1);
        continueButton1.click();
        waitForPageLoaded();

        continueButton2.click();
        paymentByCheck.click();
    }

    public void proceedAnOrder() {

        iAgreeButton.click();
        continueButtonLast.click();
    }

    public String getOrderCode() {

        return orderCode.getText().substring(17);
    }

    public FinishOrderPage(WebDriver driver) {
        super(driver);
    }
}

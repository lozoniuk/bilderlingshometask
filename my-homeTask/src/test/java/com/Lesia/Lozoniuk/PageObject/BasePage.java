package com.Lesia.Lozoniuk.PageObject;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import com.Lesia.Lozoniuk.BaseTest.ConciseAPI;
import org.openqa.selenium.support.ui.Select;

public class BasePage extends ConciseAPI{

    @Override
    public WebDriver getWebDriver() {
        return driver;
    }
    private WebDriver driver;

    public BasePage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    public void scrollToElement (WebElement elementToScroll) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].scrollIntoView();", elementToScroll);
    }

    public void selectFromDropdown(WebElement elementToSelect, int index){
        Select dropdown = new Select(elementToSelect);
        dropdown.selectByIndex(index);
    }
}

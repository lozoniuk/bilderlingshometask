package com.Lesia.Lozoniuk.Listeners;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.AbstractWebDriverEventListener;

import org.openqa.selenium.JavascriptExecutor;


public class EventListenerImplementation extends AbstractWebDriverEventListener {

    private By lastFindBy;

    /*
     *  URL NAVIGATION | NAVIGATE() & GET()
     */
    // Prints the URL before Navigating to specific URL
    @Override
    public void beforeNavigateTo(String url, WebDriver driver) {
        System.out.println("Before Navigating To : " + url + ", my url was: "
                + driver.getCurrentUrl());
    }

    // Prints the current URL after Navigating to specific URL
    @Override
    public void afterNavigateTo(String url, WebDriver driver) {
        System.out.println("After Navigating To: " + url + ", my url is: "
                + driver.getCurrentUrl());
    }

    // Prints the URL before Navigating back "navigate().back()"
    @Override
    public void beforeNavigateBack(WebDriver driver) {
        System.out.println("Before Navigating Back. I was at "
                + driver.getCurrentUrl());
    }

    // Prints the current URL after Navigating back "navigate().back()"
    @Override
    public void afterNavigateBack(WebDriver driver) {
        System.out.println("After Navigating Back. I'm at "
                + driver.getCurrentUrl());
    }

    // Prints the URL before Navigating forward "navigate().forward()"
    @Override
    public void beforeNavigateForward(WebDriver driver) {
        System.out.println("Before Navigating Forward. I was at "
                + driver.getCurrentUrl());
    }

    // Prints the current URL after Navigating forward "navigate().forward()"
    @Override
    public void afterNavigateForward(WebDriver driver) {
        System.out.println("After Navigating Forward. I'm at "
                + driver.getCurrentUrl());
    }

    /*
     * FINDING ELEMENTS | FINDELEMENT() & FINDELEMENTS()
     */
    // Called before finding Element(s)
    @Override
    public void beforeFindBy(By by, WebElement element, WebDriver driver) {
        lastFindBy = by;
        System.out.println("Trying to find: '" + lastFindBy + "'.");
    }

    // Called after finding Element(s)
    @Override
    public void afterFindBy(By by, WebElement element, WebDriver driver) {
        lastFindBy = by;
        System.out.println("Found: '" + lastFindBy + "'.");
    }

    /*
     * CLICK | CLICK()
     */
    // Called before clicking an Element
    @Override
    public void beforeClickOn(WebElement element, WebDriver driver) {
        System.out.println("Trying to click: '" + element + "'");
        // Highlight Elements before clicking
        for (int i = 0; i < 1; i++) {
            JavascriptExecutor js = (JavascriptExecutor) driver;
            js.executeScript(
                    "arguments[0].setAttribute('style', arguments[1]);",
                    element, "color: black; border: 3px solid black;");
        }
    }

    // Called after clicking an Element
    @Override
    public void afterClickOn(WebElement element, WebDriver driver) {
        System.out.println("Clicked Element with: '" + element + "'");
    }

}
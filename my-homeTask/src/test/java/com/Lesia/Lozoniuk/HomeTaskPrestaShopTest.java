package com.Lesia.Lozoniuk;

import com.Lesia.Lozoniuk.Listeners.TestNgEventListener;
import com.Lesia.Lozoniuk.BaseTest.BaseTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.util.List;


@Listeners(TestNgEventListener.class)
public class HomeTaskPrestaShopTest extends BaseTest {

    SoftAssert softAssertion = new SoftAssert();

    @Test
    public void registerInAccount() {

        shopPage.switchToFrame();
        registrationFormPage.registrationNewUser();
        softAssertion.assertTrue(registrationFormPage.getNameOfNewUser().contains(registrationFormPage.lastName), "User did not login");
    }

    @Test(dependsOnMethods = {"registerInAccount"})
    public void filteringByPrice() {
        shopPage.selectProductWithPrice18_20();
    }

    @Test(dependsOnMethods = {"filteringByPrice"})
    public void checkPricesAfterSorting() {

        List<Double> allPrices = shopPage.grabAllPrices();
        final double PRICE_MIN = 18.00;
        final double PRICE_MAX = 20.00;
        final int ITEMS_NUMBER = 2;

        for (double price : allPrices) {
            softAssertion.assertTrue(((price >= PRICE_MIN) && (price <= PRICE_MAX)), "Price is not in the range" + PRICE_MIN + "-" + PRICE_MAX);
        }
        shopPage.addToCard(ITEMS_NUMBER);
    }

    @Test(dependsOnMethods = {"checkPricesAfterSorting"})
    public void checkPricesForCorrectCalculation() {
        final int PRODUCTS_TO_ADD_COUNT = 2;
        softAssertion.assertEquals(cardPage.priceTotal(), cardPage.priceOfItem() * PRODUCTS_TO_ADD_COUNT, 0.01, "Sum of the prices of the first item in the card is not correct");
    }

    @Test(dependsOnMethods = {"checkPricesForCorrectCalculation"})
    public void navigateBackAndAddElseProductToCard() {
        final int ITEMS_NUMBER = 1;
        final int TIMES = 3;
        shopPage.navigateBack(TIMES);
        shopPage.addToCard(ITEMS_NUMBER);
    }

    @Test(dependsOnMethods = {"navigateBackAndAddElseProductToCard"})
    public void checkPriceTotalOnTheCard() {

        softAssertion.assertEquals(cardPage.getPriceAllItems(), cardPage.checkPriceTotalOnTheCard(), 0.01, "Sum of the price of the all items in the card is not correct");
    }

    @Test(dependsOnMethods = {"checkPriceTotalOnTheCard"})
    public void proceedToCheckout() {

        cardPage.proceedToCheckout();
        finishOrderPage.fillFormToCheckout();
        softAssertion.assertEquals(cardPage.getPriceTotalOnTheCard(), cardPage.getPriceAllItems() + cardPage.getShippingPrice(), 0.01, "Total price of the all items in the card is not correct");
        softAssertion.assertEquals(cardPage.getPriceTotalOnTheCard(), cardPage.getPriceInPaymentByCheck(), 0.01, "Total price of the all items on the 'payment by Check' is not correct");
        finishOrderPage.proceedAnOrder();
    }

    @Test(dependsOnMethods = {"proceedToCheckout"})
    public void checkOrderDetailsAndLogout() {

        softAssertion.assertTrue(finishOrderPage.getOrderCode().chars().allMatch(Character::isLetter), "Code of order is not correct");
        softAssertion.assertTrue(finishOrderPage.orderDetails.getText().contains(finishOrderPage.orderPaymentText), "Payment text is not correct");
        softAssertion.assertTrue(finishOrderPage.orderDetails.getText().contains(finishOrderPage.orderShippingText), "Shipping text is not correct");
        softAssertion.assertTrue(finishOrderPage.orderDetails.getText().contains(finishOrderPage.orderDeliveryText), "Delivery text is not correct");

        registrationFormPage.logOut();
        softAssertion.assertFalse(registrationFormPage.signInButton.getText().contains(registrationFormPage.firstName), "Logout is not happened");
        softAssertion.assertAll();
    }
}
